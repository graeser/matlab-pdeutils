function grid = pdetool2grid(p,e,t)
% generate grid structure from matlab pdetool-triple

grid = generateGrid(p, t(1:3,:));

