function dT = elementTransformationJacobian(grid, elementIndex)

dim = size(grid.nodes,1);
simplex = grid.nodes(:,grid.elements(1:(dim+1),elementIndex));
dT = zeros(dim,dim);

for i=1:dim
    dT(:,i) = simplex(:,i+1) - simplex(:,1);
end

