function plotGridFunction(grid, basis, u)
%plotGridFunction Plots a discrete finite element function.
%   WARNING:
%       Currently this function only works with the nodal P1/Q1 bases.
%
%   Inputs:
%       grid    Grid of the finite element space.
%       basis   Basis of the finite element space.
%       u       Vector representation of discrete function with respect
%               to the given basis and grid.
%

    dim = size(grid.nodes,1);

    if (dim==2)
        n_nodes = size(grid.nodes, 2);
        trisurf(grid.elements(1:3,:)', grid.nodes(1,:)', grid.nodes(2,:)', u(1:n_nodes), u(1:n_nodes));
    else
        error( 'Plotting in dim=%d is not supported.', ...
                dim );
    end

end

