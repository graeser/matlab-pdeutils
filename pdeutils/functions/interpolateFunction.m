function u = interpolateFunction(grid, basis, varargin)
%interpolateFunction Interpolates a function with respect to a given finite
%element space.
%
% Input:
%   grid        The grid of the finite element space.
%   basis       A basis of the finite element space.
%   [varargin]  A list of function handles that are required by the
%               localInterpolation method of the supplied basis.
%
% Output:
%   u           Vector representation of the finite element interpolation.

    % Dimension of the ansatz space.
    n   = basis.size(grid);

    % Initialize vector.
    u = zeros(n, 1);

    % Loop over all elements.
    for e = 1:size(grid.elements,2)

        % Compute local interpolation.
        uLocal = basis.localInterpolation(grid, e, varargin{:});

        % Local size.
        nLocal = basis.localSize(grid, e);

        % Compute global indices.
        for j=1:nLocal
            u(basis.index(grid, e, j)) = uLocal(j);
        end

    end
    
end
